module.exports = function() {
    //----------#BUILD FOLDER
    //config webserver for browserSync build
    var config = {
        server: {
            baseDir: $.pathObject.pathVar.path.build.serve,
            index: $.pathObject.pathVar.path.build.serveIndex,
            reloadDelay: 300,
        },
        tunnel: false,
        host: 'localhost',
        port: 666,
        logPrefix: "sharnirio"
    };
    // task webserver for browserSync  build
    $.gulp.task('webserver', function() {
        $.gp.browserSync(config);
    });

    //----------#PRODUCTION FOLDER
    //config webserver for browserSync prod
    var configProd = {
        server: {
            baseDir: $.pathObject.pathVar.path.production.serve,
            index: $.pathObject.pathVar.path.production.serveIndex,
            reloadDelay: 300,
        },
        tunnel: false,
        host: 'localhost',
        port: 999,
        logPrefix: "sharnirioProd"
    };
    // task webserver for browserSync prod
    $.gulp.task('webserverProd', function() {
        $.gp.browserSync(configProd);
    });
};