module.exports = function() {
    //----------#BUILD FOLDER
    // js file build version project
    $.gulp.task('js:build', function() {
        $.gulp.src($.pathObject.pathVar.path.src.js)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.rigger())
            .pipe($.gp.sourcemaps.write())
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.js))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });
    $.gulp.task('jsMap:build', function() {
        $.gulp.src($.pathObject.pathVar.path.src.jsMap)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.rigger())
            .pipe($.gp.sourcemaps.write())
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.js))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });

    $.gulp.task('jsLibs:build', function() {
        $.gulp.src($.pathObject.pathVar.path.src.jsLibs)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.rigger())
            .pipe($.gp.sourcemaps.write())
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.js))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });

    $.gulp.task('jsDev:build', function() {
        $.gulp.src($.pathObject.pathVar.path.src.jsDev)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.rigger())
            .pipe($.gp.sourcemaps.write())
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.js))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });

    //----------#PRODUCTION FOLDER
    // js file production version project
    $.gulp.task('js:buildProd', function() {
        $.gulp.src($.pathObject.pathVar.path.src.js)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.rigger())
            .pipe($.gp.uglify())
            .pipe($.gulp.dest($.pathObject.pathVar.path.production.js))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });
    $.gulp.task('jsMap:buildProd', function() {
        $.gulp.src($.pathObject.pathVar.path.src.jsMap)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.rigger())
            .pipe($.gulp.dest($.pathObject.pathVar.path.production.js))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });
    $.gulp.task('jsLibs:buildProd', function() {
        $.gulp.src($.pathObject.pathVar.path.src.jsLibs)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.rigger())
            .pipe($.gp.uglify())
            .pipe($.gulp.dest($.pathObject.pathVar.path.production.js))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });
};