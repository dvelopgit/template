module.exports = function() {
	//----------#BUILD FOLDER
    // watch task with build 
    $.gulp.task('watch', function() {
        $.gp.notify("Watcher is START!").write('');
        $.gulp.watch([$.pathObject.pathVar.path.watch.html], ['html:build']).on('change', function() {
            $.gp.notify("HTML file was changed!").write('');
        });
        $.gulp.watch([$.pathObject.pathVar.path.watch.style], ['css:build', 'css:buildDev']).on('change', function() {
            $.gp.notify("CSS file was changed!").write('');
        });
        $.gulp.watch([$.pathObject.pathVar.path.watch.js], ['js:build']).on('change', function() {
            $.gp.notify("JS file was changed!").write('');
        });
        $.gulp.watch([$.pathObject.pathVar.path.watch.jsLibs], ['jsLibs:build', 'jsDev:build']).on('change', function() {
            $.gp.notify("JS-LIBS file was changed!").write('');
        });
        $.gulp.watch([$.pathObject.pathVar.path.watch.fonts], ['fonts:build']).on('change', function() {
            $.gp.notify("FONTS file was changed!").write('');
        });
        $.gulp.watch([$.pathObject.pathVar.path.watch.img], ['image:build']).on('change', function() {
            $.gp.notify("IMG file was changed!").write('');
        });
    });

	//----------#PRODUCTION FOLDER
}