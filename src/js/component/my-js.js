;

//module starttimer
//= ./module/starttimer.js

jQuery(document).ready(function($) {
'use strict';
// ------------------My js-------------------------

//module for seach
//= ./module/seach-fade.js
//module for seach ----INIT
seachFade();

//module blockcollapsed for menu and other
//= ./module/blockcollapsed.js
//module blockcollapsed for menu and other ----INIT
function blockCollapsedInit() {
    var trigger = '.panelBtn';
    $(trigger).on('click', function(event) {
        event.preventDefault();
        blockCollapsed(event);
        funcMatchHeight();
    });
}
blockCollapsedInit();

//module blockcollapsed for menu and other
//= ./module/resizeblockcollapsed.js
//module blockcollapsed for menu and other ----INIT
$(window).on('resize orientationchange', function() {
    resizeblockCollapsed();
    ajaxStopFun();
});


//module magnific popup
//= ./module/magnific-popup.js
//module magnific popup  ----INIT
funcPopup();

//jquery-match-height
//= ./module/match-height.js

//scrollbar
//= ./module/scrollbar.js
//scrollbar ----INIT
myScrollbar();

//style-inpyt
//= ./module/style-inpyt.js
//style-inpyt  ----INIT
styleInput();

//function ajaxStop
function ajaxStopFun() {
    $(document).ajaxStop(function() {
        funcPopup();
        funcMatchHeight();
    });
};
ajaxStopFun();

// ------------------My js END-------------------------
});
