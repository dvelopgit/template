//module for seach
function seachFade() {
    var searcHeader = $(".header .btn-search");
    var searcInHeader = $(".header .search-in");
    searcHeader.click(function(e) {
        $(this).toggleClass("on");
        if (searcInHeader.css('display') == 'none') {
            searcInHeader.fadeIn(200);
        } else {
            searcInHeader.fadeOut(200);
        }
        e.preventDefault();
    });
    // закрытие поиска по клику в любом месте
    function clickDocument(event) {
        var clickDoc = $('.clickDoc');
        var div2 = $('header .btn-search')
        if (!clickDoc.is(event.target) // если клик был не по блоку
            &&
            clickDoc.has(event.target).length === 0) { // и не по его дочерним элементам
            clickDoc.fadeOut(200);
            div2.removeClass("on");
        }
    }
    $(document).mouseup(function(event) { // событие клика по веб-документу
        clickDocument(event)
    });

};