/* Popup
* Website: http://dimsemenov.com/plugins/magnific-popup/
==========================================================================
*/
function funcPopup() {
    // Image popups fancy box
    // var gImg = $('a[href*="img"]');
    // $(gImg).magnificPopup({
    //     // delegate: 'a',
    //     type: 'image',
    //     removalDelay: 500,
    //     gallery: {
    //         enabled: true,
    //         navigateByImgClick: true,
    //         preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    //     },
    //     image: {
    //         tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
    //     },
    //     callbacks: {
    //         beforeOpen: function() {
    //             // just a hack that adds mfp-anim class to markup
    //             this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
    //             this.st.mainClass = this.st.el.attr('data-effect');
    //         }
    //     },
    //     closeOnContentClick: true,
    //     midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    // });

    $('.popup-show').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        callbacks: {
            beforeOpen: function() {
                if ($(window).width() < 700) { this.st.focus = false; } else { this.st.focus = '#name'; }
            }
        }
    });
    $(document).on('click', '.mfp-close', function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    $(document).on('click', '.popup-close', function(e) {
        e.preventDefault();
        $.magnificPopup.close();
        location.reload();
    });
    if ($('.feedback-2').css('display') == 'block') { $('body').addClass('wrappOver'); } else { $('body').removeClass('wrappOver'); }
    $(document).on('click', '.hid-popup', function(e) {
        $('body').removeClass('wrappOver');
        $('.feedback-2').css('display', 'none');
        location.reload();
    });
};