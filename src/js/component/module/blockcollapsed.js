function blockCollapsed(event) {
    var trigger = '.panelBtn',
        menuToggle = '.panelToggle',
        wrapParent = '.panelWrapper',
        $_menuItem = $(event.target).parent(wrapParent),
        $_panel = $_menuItem.find(menuToggle);
    if ($_menuItem.data('collapsed')) {
        $_menuItem.data('collapsed', false).removeClass('js-open').addClass('js-closed');
        $_panel.slideUp();
    } else {
        $_menuItem.data('collapsed', true).removeClass('js-closed').addClass('js-open');
        $_panel.slideDown();
    }
}